const express = require('express');

const route = express.Router();

const authController = require('../controllers/auth.controller')
const authMiddleware = require('../middlewares.js/auth.middleware')



//hàm signUp chỉ dùng cho user
route.post('/signup', authMiddleware.checkDuplicateUsername, authMiddleware.checkDuplicateEmail,authController.signUp)

route.post('/login', authController.logIn);

route.post("/refreshToken", authController.refreshToken);


module.exports = route;