const express = require('express');

const productRoute = express.Router();

const productController = require("../controllers/product.controller")
const productMiddleware = require('../middlewares.js/product.middleware')

productRoute.post('/products',productMiddleware.verifyToken, productMiddleware.checkUser, productController.createProduct)

productRoute.get('/products', productMiddleware.verifyToken, productController.getAllProducts)

productRoute.get('/products/:productId',productMiddleware.verifyToken, productMiddleware.checkUser, productController.getProductById)

productRoute.put('/products/:productId',productMiddleware.verifyToken, productMiddleware.checkUser, productController.updateProductById)

productRoute.delete('/products/:productId',productMiddleware.verifyToken, productMiddleware.checkUser, productController.deleteProductById)

module.exports = { productRoute } 