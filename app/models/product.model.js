const { timeStamp } = require('console');
const mongoose = require('mongoose');


const productModel = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    price: {
        type: Number,
        require: true
    }
},
   { timeStamp: true}
)

module.exports = mongoose.model("Product", productModel)