const db = require("../models")
const mongoose = require('mongoose')

const createProduct = async (req, res) => {
    const { name, price } = req.body;
    if (!name) {
        return res.status(400).json({
            status: 'Bad request',
            message: `name id required`
        })
    }
    if (!price) {
        return res.status(400).json({
            status: 'Bad request',
            message: `price id required`
        })
    }
    try {
        const productCreated = new db.product({
            name,
            price
        });
        await productCreated.save();
        return res.status(201).json({
            status: `Create new product ${name} successfully`,
            data: productCreated
        })
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }

}

const getAllProducts = async (req, res) => {

    try {
        const productList = await db.product.find();
        if (productList && productList.length > 0) {
            return res.status(200).json({
                status: `Get all products successfully !`,
                data: productList
            })
        } else {
            return res.status(404).json({
                status: `Not found any products!`,
                data: productList
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const getProductById = async (req, res) => {
    //collect data:
    const productId = req.params.productId;
    //validate data
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Product Id ${productId} is invalid!`
        })
    }
    try {
        const productFoundById = await db.product.findById(productId);
        if (productFoundById) {
            return res.status(200).json({
                status: `Get product by Id ${productId} successfully !`,
                data: productFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any products by Id ${productId} !`,
                data: productFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const updateProductById = async (req, res) => {
    //collect data:
    const productId = req.params.productId;
    const { name, price } = req.body;

    //validate:
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Product Id ${productId} is invalid!`
        })
    }
    if (!name) {
        return res.status(400).json({
            status: 'Bad request',
            message: `name id required`
        })
    }
    if (!price) {
        return res.status(400).json({
            status: 'Bad request',
            message: `price id required`
        })
    }
    const productUpdateData = {
        name,
        price
    }
    //use model
    try {
        const productUpdated = await db.product.findByIdAndUpdate(productId, productUpdateData);
        if (productUpdated) {
            const newProductUpdated = await db.product.findOne({ name })
            return res.status(200).json({
                status: `Update product by id ${productId} successfully`,
                data: newProductUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any products by Id ${productId} !`,
                data: productFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const deleteProductById = async (req, res) => {
    //collect data:
    const productId = req.params.productId;

    //validate:
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Product Id ${productId} is invalid!`
        })
    }
    try {
        const productDeleted = await db.product.findByIdAndDelete(productId);
        if (productDeleted) {
            return res.status(200).json({
                status: `Delete product by id ${productId} successfully`,
            })
        } else {
            return res.status(404).json({
                status: `Not found any products by Id ${productId} !`,
                data: productDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }

}
module.exports = {
    createProduct,
    getAllProducts,
    getProductById,
    updateProductById,
    deleteProductById

}