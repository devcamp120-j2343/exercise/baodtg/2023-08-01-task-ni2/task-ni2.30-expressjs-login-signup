const db = require('../models');
const jwt = require("jsonwebtoken");

const User = db.user;
const Role = db.role;
// var tokenDangNhap = "";
// const checkExpiredToken = async (req, res, next) => {
//     try {
//         console.log(`Check expire token...`);
//         const token = req.headers['x_access_token'];
//         const refreshToken = req.headers['x_refresh_token'];
//         if (token == null) {
//             console.log("check thoi han fail vi k co access token")
//             return res.status(401).send({
//                 message: "Không tìm thấy x-access-token!"
//             });
//         }
//         // const decodedToken = jwt.decode(token);
//         // console.log(decodedToken)
//         // const accessTokenExpireTime = await decodedToken.exp * 1000 //*1000 để ra đúng thời gian thực
//         // //thời gian hiện tại
//         // const timeNow = new Date().getTime()
//         // // Nếu thời hạn token <= thời gian hiện tại thì gọi API refresh Token:
//         // if
//         //     (accessTokenExpireTime > timeNow) {
//         //     console.log("token còn hạn")
//         //     tokenDangNhap = await req.headers['x_access_token'];
//         //     next()
//         // }
//         // else {
//         //     console.log("token hết hạn")
//         //     //call api to refresh token:
//         //     const response = await fetch("http://localhost:8080/api/auth/refreshToken", {
//         //         method: "POST",
//         //         headers: { "Content-Type": "application/json" },
//         //         body: JSON.stringify({ refreshToken: refreshToken })
//         //     })
//         //     const newAccessToken = await response.json();
//         //     tokenDangNhap = newAccessToken.accessToken
//         //     next()
//         // }
//         next()
//     } catch (error) {
//         console.log(error)
//     }

// }

const verifyToken = async (req, res, next) => {
    try {
        console.log(`Verify token...`);
        const token = req.headers['x_access_token'];
        if (!token) {
            return res.status(401).send({
                message: "Không tìm thấy x-access-token!"
            });
        }
        const decodedToken = await jwt.decode(token);
        console.log(decodedToken)
        const accessTokenExpireTime = await decodedToken.exp * 1000 //*1000 để ra đúng thời gian thực
        //thời gian hiện tại
        const timeNow = new Date()
        if (accessTokenExpireTime < timeNow) {
            return res.status(401).send({
                message: "AccessToken Expired"
            });
        }


        const secretKey = process.env.JWT_SECRET;

        const verified = await jwt.verify(token, secretKey);
        console.log(verified);
        if (!verified) {
            return res.status(401).send({
                message: "x-access-token không hợp lệ!"
            });
        }

        const user = await User.findById(verified.id).populate('roles');
        // console.log(user);
        req.user = user
        next()
    } catch (error) {
        console.log(error)
        return res.status(401).send({
            message: "Access token expired"
        });
    }
}

const checkUser = (req, res, next) => {
    console.log("Check user...");
    const userRoles = req.user.roles;
    if (userRoles) {
        for (let i = 0; i < userRoles.length; i++) {
            if (userRoles[i].name == 'admin') {
                console.log("Authorized!");
                next();
                return;
            } else {
                console.log("Unauthorized");
                return res.status(401).json({
                    message: "Bạn không có quyền truy cập!"
                });
            }

        }
    }


}

module.exports = {
    verifyToken,
    checkUser,

}