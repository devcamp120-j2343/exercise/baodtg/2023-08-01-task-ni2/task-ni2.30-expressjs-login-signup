const db = require("../models");
const { v4: uuidv4 } = require("uuid")

const createToken = async (user) => {
    let expireAt = new Date();

    expireAt.setSeconds(
        expireAt.getSeconds() + Number(process.env.JWT_REFRESH_EXPIRATION)
    )

    let token = uuidv4();

    let refreshTokenObj = new db.refreshToken({
        token: token,
        user: user._id,
        expiredDate: expireAt.getTime()
    })
    const refreshToken = await refreshTokenObj.save();

    return refreshToken.token;
}

module.exports = {
    createToken
}