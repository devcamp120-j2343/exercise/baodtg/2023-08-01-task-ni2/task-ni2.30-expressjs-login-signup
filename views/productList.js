/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
"use strict";
$(document).ready(function () {
    const gBASE_URL = "http://localhost:8080/api/";
    var gSTT = 1;
    var gProductDataTable = $("#product-table").DataTable({
        columns: [
            { data: "id" },
            { data: "name" },
            { data: "price" },
            { data: "action" }
        ],
        columnDefs: [
            {
                target: 0,
                render: function () {
                    return gSTT++
                }
            },
            {
                target: 3,
                "defaultContent": `<i class="far fa-edit text-primary" id="btn-edit-product" style="font-size: 20px; cursor: pointer;"  title="Edit Course"></i>&nbsp; 
                <i class="fas fa-trash-alt text-danger" id="btn-delete-product" style="font-size: 20px; cursor: pointer;" data-toggle="tooltip" title="Delete Course"></i>
                `
            },
        ]
    });
    const gAccessToken = sessionStorage.getItem("x_access_token");
    const gRefeshToken = sessionStorage.getItem("x_refresh_token");
    const newAccessToken = "";
    var gProductId = ""
    var gProductListObj = [];
    var gProductDataObj = {
        name: "",
        price: ""
    }

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageLoading()
    //thực thi khi ấn nút New Product:
    $('#btn-add-product').on("click", () => {
        $("#modal-add-product").modal("show")
    })

    ////thực thi khi ấn nút Add Product (trên modal):
    $("#modal-add-product").on("click", "#btn-confirm-add-product", () => {
        onBtnAddProductClick()
    })

    //thực thi ấn nút update product:
    $("#product-table").on("click", "#btn-edit-product", function () {
        onBtnUpdateProductClick(this)
    });

    //thực thi nút confirm update product trên modal
    $("#modal-update-product").on("click", "#btn-confirm-update-product", function () {
        onBtnConfirmUpdateProductClick(gProductDataObj);
    });

    //thực thi ấn nút delete product:
    $("#product-table").on("click", "#btn-delete-product", function () {
        onBtnDeleteProductClick(this)
    });

    //thực thi nút confirm delete course trên modal
    $("#delete-confirm-modal").on("click", "#btn-confirm-delete-course", function () {
        onBtnConfirmDeleteProductClick();
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        callApiToGetProductList()
    }

    //hàm goi api load product list:
    async function callApiToGetProductList() {

        const response = await fetch(gBASE_URL + "products", {
            method: "GET",
            headers: {
                'x_access_token': gAccessToken,
                'x_refresh_token': gRefeshToken
            },
        })
        const data = await response.json()
        const resStatus = response.status
        console.log(resStatus)


        if (data.message == "AccessToken Expired") {
            console.log("nếu access token expired tiến hành gọi api refresh token...")
            await callApiToReFreshToken()
            const newAccessToken = await sessionStorage.getItem("x_access_token")
            const response = await fetch(gBASE_URL + "products", {
                method: "GET",
                headers: {
                    'x_access_token': newAccessToken,
                    'x_refresh_token': gRefeshToken
                },
            })
            const data = await response.json();
            gProductListObj = data.data
            loadDataToTable(gProductListObj)
            console.log(data)
        }

        else if (resStatus == 200) {
            gProductListObj = data.data
            loadDataToTable(gProductListObj)
        }

        else {
            window.location.href = "loginPage.html"
        }
    }
    //hàm ấn nút confirm add product (trên modal)
    const onBtnAddProductClick = () => {
        //B1: colect data:
        collectProductData(gProductDataObj);
        //B2: validate:
        var vDataValid = validateProductAddData(gProductDataObj);
        if (vDataValid) {
            //B3: call Api to create new product
            callApiToCreateProduct(gProductDataObj)
        }
    }
    //hàm ấn nút Update product
    const onBtnUpdateProductClick = (paramUpdateBtn) => {
        gProductDataObj = getProductData(paramUpdateBtn);
        loadProductDataToModal(gProductDataObj);
        $("#modal-update-product").modal("show")
    }
    //hàm ấn nút confirm update product trên modal:
    const onBtnConfirmUpdateProductClick = (paramProductUpdateData) => {
        //collect data:
        getProductUpdateData(paramProductUpdateData)
        //validate data: 
        const dataValid = validateProductUpdateData(paramProductUpdateData)
        if (dataValid) {
            //call API to update Product:
            callApiToUpdateProduct(paramProductUpdateData)
        }
    }

    //Hàm ấn nút delete product:
    const onBtnDeleteProductClick = (paramBtn) => {
        var vTableRow = $(paramBtn).parents("tr");
        var vDataTableRow = gProductDataTable.row(vTableRow);
        var vProductData = vDataTableRow.data();
        gProductId = vProductData._id;

        $("#delete-confirm-modal").modal("show");
    }

    //hàm ấn nút confirm delete course trên modal: 
    const onBtnConfirmDeleteProductClick = async () => {
        try {
            console.log("xoa xoa")
            console.log(gProductId)
            const res = await fetch(gBASE_URL + "products/" + gProductId, {
                method: "DELETE",
                headers: {
                    "x_access_token": gAccessToken,
                    'Content-Type': 'application/json'
                }
            })
            const data = await res.json();

            const resStatus = (await res).status;
            // console.log(`ket qua tra ve:  + ${data.message}`)

            if (resStatus == 401 && data.message == "AccessToken Expired") {
                console.log("nếu access token expired tiến hành gọi api refresh token...")
                await callApiToReFreshToken()
                const newAccessToken = await sessionStorage.getItem("x_access_token")
                const res = fetch(gBASE_URL + "products/" + gProductId, {
                    method: "DELETE",
                    headers: {
                        "x_access_token": newAccessToken,
                        'Content-Type': 'application/json'
                    }
                })
                    alert("Delete product successfully!");
                    $("#delete-confirm-modal").modal("hide");
                    window.location.reload()
                

            }

            else if (resStatus == 200) {
                alert("Delete product successfully!");
                $("#delete-confirm-modal").modal("hide");
                window.location.reload()
            } else {
                alert("Tài khoản của bạn không có quyền thực hiện thao tác này!")
                window.location.href = "loginPage.html"
            }
        } catch (error) {
            console.log(error)
        }

    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //hàm load product data to table:
    function loadDataToTable(paramData) {
        gProductDataTable.rows.add(paramData);
        gProductDataTable.draw();
    }
    //các hàm thực thi add product:
    //B1: colect data:
    const collectProductData = (paramProductData) => {
        paramProductData.name = $("#inp-product-name").val().trim();
        paramProductData.price = $("#inp-price").val().trim();
    }
    //B2: validate:
    const validateProductAddData = (paramProductData) => {
        if (!paramProductData.name) {
            alert(`Please input product name !`)
            return false
        }
        if (!paramProductData.price) {
            alert(`Please input price !`)
            return false
        }
        if (isNaN(paramProductData.price)) {
            alert(`Price is a number !`)
            return false
        }
        return true
    }
    //B3: call Api to create new product
    const callApiToCreateProduct = async (paramProductData) => {
        const res = await fetch(gBASE_URL + "products", {
            method: "POST",
            headers: {
                "x_access_token": gAccessToken,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(paramProductData)
        })
        const data = await res.json();
        const resStatus = res.status

        //nếu access token expired sẽ tiến hành gọi api refresh token
        if (data.message == "AccessToken Expired") {
            console.log("nếu access token expired tiến hành gọi api refresh token...")
            await callApiToReFreshToken()
            const newAccessToken = await sessionStorage.getItem("x_access_token")
            const res = await fetch(gBASE_URL + "products", {
                method: "POST",
                headers: {
                    "x_access_token": newAccessToken,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(paramProductData)
            })
            const data = await res.json();
            handleProductAdded();
            console.log(data)
        }


        else if (resStatus == 201) {
            //B4: handle product added:
            console.log(data)
            console.log("nếu access token còn hạn tiến hành gọi api add product...")

            handleProductAdded();
        } else {
            console.log(data)
            alert("Tài khoản của bạn không có quyền thực hiện thao tác này!")
            window.location.href = "loginPage.html"
        }
    }
    //B4: handle product added:
    const handleProductAdded = () => {
        alert("Create new product successfully !")
        $("#modal-add-product").modal("hide");
        window.location.reload()
    };

    //vùng chứa hàm xử lý edit product:
    const getProductData = (paramBtn) => {
        var vTableRow = $(paramBtn).parents("tr");
        var vDataTableRow = gProductDataTable.row(vTableRow);
        var vProductData = vDataTableRow.data();
        gProductId = vProductData._id;
        return vProductData
    }
    //hàm load course data to modal
    const loadProductDataToModal = (paramProductDataObj) => {
        $("#inp-product-name-update").val(paramProductDataObj.name)
        $("#inp-price-update").val(paramProductDataObj.price)
    }

    //collect product update data:
    const getProductUpdateData = (paramProductUpdateData) => {
        paramProductUpdateData.name = $("#inp-product-name-update").val().trim();
        paramProductUpdateData.price = $("#inp-price-update").val().trim();

    }

    //validate product update data:
    const validateProductUpdateData = (paramProductUpdateData) => {
        if (!paramProductUpdateData.name) {
            alert(`Please input product name !`)
            return false
        }
        if (isNaN(paramProductUpdateData.price)) {
            alert(`Price is a number !`)
            return false
        }
        return true
    }
    //call API to update Product:
    const callApiToUpdateProduct = async (paramProductUpdateData) => {
        try {
            const res = await fetch(gBASE_URL + "products/" + gProductId, {
                method: "PUT",
                headers: {
                    "x_access_token": gAccessToken,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(paramProductUpdateData)
            })
            const data = await res.json();
            const resStatus = res.status


            if (data.message == "AccessToken Expired") {
                console.log("nếu access token expired tiến hành gọi api refresh token...")
                await callApiToReFreshToken()
                const newAccessToken = await sessionStorage.getItem("x_access_token")
                const res = await fetch(gBASE_URL + "products/" + gProductId, {
                    method: "PUT",
                    headers: {
                        "x_access_token": newAccessToken,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(paramProductUpdateData)
                })
                const data = await res.json();
                handleProductUpdated();
                console.log(data)
            }


            else if (resStatus == 200) {

                console.log(data)
                //handle product updated
                handleProductUpdated()

            } else {
                alert("Tài khoản của bạn không có quyền thực hiện thao tác này!")
                window.location.href = "loginPage.html"
            }

        } catch (error) {
            console.log(error)
            alert("ban khong co quyên")

            // window.location.href = "loginPage.html"
        }
    }
    //handle product updated
    const handleProductUpdated = () => {
        alert("Update product successfully!");
        $("#modal-update-product").modal("hide");
        window.location.reload()
    }
    //hàm gọi api to refresh token
    const callApiToReFreshToken = async () => {
        const response = await fetch("http://localhost:8080/api/auth/refreshToken", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ refreshToken: gRefeshToken })
        })
        const data = await response.json();
        sessionStorage.setItem("x_access_token", data.accessToken);
        sessionStorage.setItem("x_refresh_token", data.refreshToken);


    }







});