/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gUserObj = {
    userName: "",
    email: "",
    password: "",
    confirmPassword: ""
}

var gUserLogInObj = {
    userName: "",
    password: "",
}
const gBASE_URL = "http://localhost:8080/api/auth/"
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$('#sign-up-form').on("submit", (paramsBtn) => {
    paramsBtn.preventDefault();
    onBtnSignUpClick(gUserObj)
})

$('#log-in-form').on("submit", (paramsBtn) => {
    paramsBtn.preventDefault();
    onBtnLogInClick(gUserLogInObj)
    console.log(gUserLogInObj)
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
onBtnSignUpClick = (paramUserObj) => {
    //thu thập dữ liệu:
    paramUserObj.userName = $('#inp-username-signup').val().trim();
    paramUserObj.email = $('#inp-email-signup').val().trim();
    paramUserObj.password = $('#inp-password-signup').val().trim();
    paramUserObj.confirmPassword = $('#inp-confirm-password-signup').val().trim();
    //validate:
    var vDataValid = validateSignUpForm(paramUserObj)
    if (vDataValid) {
        //B3: xử lý data
        callApiToSignUpUser(paramUserObj)

    }

}

onBtnLogInClick = (paramUserObj) => {
    //thu thập dữ liệu:
    paramUserObj.userName = $('#inp-userName').val().trim();
    paramUserObj.password = $('#inp-password').val().trim();
    //validate:
    var vDataValid = validateLogInForm(paramUserObj)
    if (vDataValid) {
        //B3: xử lý data
        callApiToLoginUser(paramUserObj)

    }
    //B3: xử lý data


}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
const validateSignUpForm = (paramUserObj) => {
    if (!paramUserObj.userName) {
        alert("Please input Username !")
        return false
    }
    if (!validateEmail(paramUserObj.email)) {
        alert("Email is invalid !")
        return false
    }
    if (!paramUserObj.password || paramUserObj.password.length < 6) {
        alert("Password included aleast 6 characters !")
        return false
    }
    if (paramUserObj.confirmPassword != paramUserObj.password) {
        alert("Password confirm must same with password !")
        return false
    }
    return true
}
//B3: xử lý data
const callApiToSignUpUser = async (paramUserObj) => {
    try {
        const res = await fetch(gBASE_URL + "signup", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(paramUserObj)
        })
        const data = await res.json()
        if (data.message == "Create user successfully") {
            console.log(data)
            //B4: xử lý hiển thị:
            handleUserSignedUp(paramUserObj)
        } else if (data.message == 'Username is already in use') {
            alert(data.message)
        }
        else if (data.message == 'Email is already in use') {
            alert(data.message)
        }
    } catch (error) {
        console.log(error)
    }

}


const handleUserSignedUp = (paramUserObj) => {
    alert("Sign up user successfully !")
    window.location.href = "http://localhost:8080/"
}

// hàm xử lý log in form
const validateLogInForm = (paramUserObj) => {
    if (!paramUserObj.userName) {
        alert("Please input Username !")
        return false
    }

    if (!paramUserObj.password) {
        alert("Please input password !")
        return false
    }

    return true
}

//B3: xử lý data
const callApiToLoginUser = async (paramUserObj) => {
    try {
        const res = await fetch(gBASE_URL + "login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(paramUserObj)
        })
        const data = await res.json()
        if (data) {
            console.log(data.accessToken)
            handleUserLogin(data)
        }
       
    } catch (error) {
        console.log(error)
    }

}

const handleUserLogin = (paramData) => {
    sessionStorage.setItem("x_access_token", paramData.accessToken),
    sessionStorage.setItem("x_refresh_token", paramData.refreshToken)
    // const a = sessionStorage.getItem("x_access_token");
    // console.log(a)
    window.location.href = "productPage.html"
}

const validateEmail = (paramEmail) => {
    var Regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (paramEmail.match(Regex)) {
        return true
    } else {
        return false

    }
}
