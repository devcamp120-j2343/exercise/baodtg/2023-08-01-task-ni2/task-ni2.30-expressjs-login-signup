const express = require("express");
const path = require('path')
const cors = require('cors');

require('dotenv').config();

const db = require('./app/models')


const { initial } = require("./data");
const { route } = require("./app/routes/product.routes")

const app = express();

app.use(cors());
app.use(express.json())

db.mongoose
    .connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`)
    .then(() => {
        console.log("Connect mongoDB Successfully");
        initial()
    })
    .catch((err) => {
        console.error('Connection error', err);
        process.exit();
    })



app.use(express.static(__dirname + "/views"))

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + "/views/loginPage.html"))
})

app.get('/signup', (req, res) => {
    res.sendFile(path.join(__dirname + "/views/signupPage.html"))
})
//api auth
app.use('/api/auth', require('./app/routes/auth.routes'));
//api product
app.use('/api/', (require('./app/routes/product.routes')).productRoute);

const PORT = process.env.ENV_PORT || 8000;


app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`)
})